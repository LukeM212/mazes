type Vector<T, N extends number> = T[] & {length: N};
export default Vector;

export function add<N extends number>(a: Vector<number, N>, b: Vector<number, N>): Vector<number, N> {
	return a.map((v, i) => v + b[i]) as Vector<number, N>;
}

export function sub<N extends number>(a: Vector<number, N>, b: Vector<number, N>): Vector<number, N> {
	return a.map((v, i) => v - b[i]) as Vector<number, N>;
}

export function mult<N extends number>(a: Vector<number, N>, n: number): Vector<number, N> {
	return a.map(v => v * n) as Vector<number, N>;
}
