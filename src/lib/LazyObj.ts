export default class LazyObj<T> {
	private readonly value: Obj<Promise<T>> = {};
	
	constructor(
		private f: (key: string) => Promise<T>,
	) { }
	
	public async get(key: string) {
		if (key in this.value) return await this.value[key];
		else return await (this.value[key] = this.f(key));
	}
}
