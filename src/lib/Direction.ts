export const Up = [0, -1] as [0, -1] & {length: 2};
export const Down = [0, 1] as [0, 1] & {length: 2};
export const Left = [-1, 0] as [-1, 0] & {length: 2};
export const Right = [1, 0] as [1, 0] & {length: 2};

export const All = [Up, Down, Left, Right];

type Direction = ([0, -1] | [0, 1] | [-1, 0] | [1, 0]) & {length: 2};
export default Direction;
