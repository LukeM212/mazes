declare type Obj<T> = {
	[key: string]: T,
};
