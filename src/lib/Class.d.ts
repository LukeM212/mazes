declare type Class<C, PS extends any[] = []> = {
	new(...props: PS): C,
};
