export function* filter<T>(f: (item: T) => boolean, a: Iterable<T>): Iterable<T> {
	for (const item of a)
		if (f(item))
			yield item;
}

export function* map<T, U>(f: (item: T) => U, a: Iterable<T>): Iterable<U> {
	for (const item of a)
		yield f(item);
}

export function* shuffle<T>(a: Iterable<T>): Iterable<T> {
	const data = [...a];
	
	for (let n = data.length; n > 0; n--) {
		const i = Math.floor(Math.random() * n);
		if (i === n - 1) yield data.pop()!;
		else {
			const next = data[i];
			data[i] = data.pop()!;
			yield next;
		}
	}
}
