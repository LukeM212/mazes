import Vector from './Vector';

type UntypedGetIndex = (...indices: number[]) => number;

export default class MultiArray<T, N extends number> extends Array<T> {
	public readonly dimensions: Vector<number, N>;
	
	constructor(...dimensions: Vector<number, N>) {
		super(dimensions.reduce((a, b) => a * b, 1));
		
		this.dimensions = dimensions;
	}
	
	public get(...indices: Vector<number, N>) {
		const index = (this.getIndex as UntypedGetIndex)(...indices);
		return index !== undefined ? this[index] : undefined;
	}
	
	public set(value: T, ...indices: Vector<number, N>) {
		const index = (this.getIndex as UntypedGetIndex)(...indices);
		if (index !== undefined) this[index] = value;
	}
	
	public getIndex(...indices: Vector<number, N>): number | undefined {
		if (indices.every((x, i) => x >= 0 && x < this.dimensions[i]))
			return indices.reduce((a, x, i) => a * this.dimensions[i] + x, 0);
		else return undefined;
	}
}
