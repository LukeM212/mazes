export type CustomElement<C extends HTMLElement & P, P = {}> = (props: P) => C;

export function customElement<C extends HTMLElement & P, P = {}>(name: string, definition: Class<C>): CustomElement<C, P> {
	customElements.define(name, definition);
	
	return (props: P): C => Object.assign(document.createElement(name) as C, props);
}

export function extendedElement<C extends B & P, B extends HTMLElement, P = {}>(name: string, base: string, definition: Class<C>): CustomElement<C, P> {
	customElements.define(name, definition, {extends: base});
	
	return (props: P): C => Object.assign(document.createElement(base, {is: name}) as C, props);
}
