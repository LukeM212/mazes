import {customElement} from '../lib/customElement';

export type Settings = Obj<Setting>;

export type Setting = NumberSetting | CheckboxSetting | OptionsSetting;

export type NumberSetting = {
	type: 'number',
	label: string,
	min?: number,
	max?: number,
	step?: number,
	value?: number,
};

export type CheckboxSetting = {
	type: 'checkbox',
	label: string,
	value?: boolean,
	checked?: Settings,
	unchecked?: Settings,
};

export type Options = Obj<Option>;

export type Option = string | {
	label: string,
	content: Settings,
};

export type OptionsSetting = {
	type: 'options',
	label: string,
	value?: string,
	options: Options,
};

interface SettingContent {
	readonly elem: HTMLElement;
	
	serialise(to: Obj<any>, key: string): void;
}

class NumberSettingContent implements SettingContent {
	public readonly elem: HTMLDivElement;
	
	private readonly label: HTMLLabelElement;
	private readonly input: HTMLInputElement;
	
	constructor(setting: NumberSetting) {
		this.elem = document.createElement('div');
		
		this.elem.appendChild(this.label = document.createElement('label'));
		this.label.innerText = setting.label;
		
		this.elem.appendChild(this.input = document.createElement('input'));
		this.input.type = 'number';
		
		if (setting.min !== undefined) this.input.min = setting.min.toString();
		if (setting.max !== undefined) this.input.max = setting.max.toString();
		if (setting.step !== undefined) this.input.step = setting.step.toString();
		if (setting.value !== undefined) this.input.value = setting.value.toString();
	}
	
	serialise(to: Obj<any>, key: string) {
		const value = Number(this.input.value);
		
		to[key] = value;
	}
}

class CheckboxSettingContent implements SettingContent {
	public readonly elem: HTMLDivElement;
	
	private readonly label: HTMLLabelElement;
	private readonly input: HTMLInputElement;
	
	private readonly checked: SettingsContent;
	private readonly unchecked: SettingsContent;
	
	constructor(setting: CheckboxSetting) {
		this.elem = document.createElement('div');
		
		this.elem.appendChild(this.label = document.createElement('label'));
		this.label.innerText = setting.label;
		
		this.elem.appendChild(this.input = document.createElement('input'));
		this.input.type = 'checkbox';
		
		if (setting.value !== undefined) this.input.checked = setting.value;
		
		this.elem.appendChild((this.checked = new SettingsContent(setting.checked ?? {})).elem);
		this.elem.appendChild((this.unchecked = new SettingsContent(setting.unchecked ?? {})).elem);
		
		this.update();
		
		this.input.oninput = this.update.bind(this);
	}
	
	serialise(to: Obj<any>, key: string) {
		const value = this.input.checked;
		
		to[key] = value;
		
		const content = value ? this.checked : this.unchecked;
		
		content.serialise(to);
	}
	
	private update() {
		this.checked.elem.hidden = !(this.unchecked.elem.hidden = this.input.checked);
	}
}

class OptionsSettingContent implements SettingContent {
	public readonly elem: HTMLDivElement;
	
	private readonly label: HTMLLabelElement;
	private readonly input: HTMLSelectElement;
	
	private readonly options: Obj<SettingsContent>;
	
	private selected: string;
	
	constructor(setting: OptionsSetting) {
		this.elem = document.createElement('div');
		
		this.elem.appendChild(this.label = document.createElement('label'));
		this.label.innerText = setting.label;
		
		this.elem.appendChild(this.input = document.createElement('select'));
		
		this.options = {};
		
		for (const key in setting.options) {
			const value = setting.options[key];
			
			const {label, content}: {label: string, content: Settings} =
				typeof value === 'string' ? {label: value, content: {}} : value;
			
			const option = document.createElement('option');
			option.value = key;
			option.innerText = label;
			this.input.appendChild(option);
			
			const settings = this.options[key] = new SettingsContent(content);
			settings.elem.hidden = true;
			this.elem.appendChild(settings.elem);
		}
		
		this.options[this.selected = this.input.value].elem.hidden = false;
		
		this.input.oninput = this.update.bind(this);
	}
	
	serialise(to: Obj<any>, key: string) {
		const value = this.input.value;
		
		to[key] = value;
		
		const content = this.options[value];
		
		content.serialise(to);
	}
	
	private update() {
		this.options[this.selected].elem.hidden = true;
		this.options[this.selected = this.input.value].elem.hidden = false;
	}
}

class SettingsContent {
	public readonly elem: HTMLDivElement;
	private readonly content: Obj<SettingContent>;
	
	constructor(settings: Settings) {
		this.elem = document.createElement('div');
		this.content = {};
		
		for (const key in settings) {
			const value = settings[key];
			
			let content: SettingContent;
			
			switch (value.type) {
				case 'number': content = new NumberSettingContent(value); break;
				case 'checkbox': content = new CheckboxSettingContent(value); break;
				case 'options': content = new OptionsSettingContent(value); break;
			}
			
			this.elem.appendChild(content.elem);
			this.content[key] = content;
		}
	}
	
	serialise(to: Obj<any>) {
		for (const key in this.content) {
			const value = this.content[key];
			
			value.serialise(to, key);
		}
	}
}

export type SettingsProperties = {
	settings: Settings,
};

export type SettingValues = Obj<SettingValue>;

export type SettingValue = number | boolean | string;

class SettingsElement extends HTMLElement {
	private readonly shadow: ShadowRoot = this.attachShadow({mode: 'closed'});
	private content: SettingsContent = new SettingsContent({});
	
	public set settings(settings: Settings) {
		const content = new SettingsContent(settings);
		this.shadow.replaceChild(content.elem, this.content.elem);
		this.content = content;
	}
	
	public get values() {
		const result: SettingValues = {};
		this.content.serialise(result);
		return result;
	}
	
	constructor() {
		super();
		
		this.shadow.appendChild(this.content.elem);
	}
}

export default customElement<SettingsElement, SettingsProperties>('x-settings', SettingsElement);
