import {customElement, CustomElement} from '../lib/customElement';
import MultiArray from '../lib/MultiArray';
import Direction from '../lib/Direction';

export type MazeProperties<T> = {
	width: number,
	height: number,
	tileSize?: number,
	wallSize?: number,
	colours?: Map<T, string> | ((c: T) => string),
};

export class MazeElement<T = boolean> extends HTMLElement {
	private readonly shadow: ShadowRoot = this.attachShadow({mode: 'closed'});
	
	private readonly canvas: HTMLCanvasElement;
	private readonly ctx: CanvasRenderingContext2D;
	
	public maze: MultiArray<T, 2>;
	
	private _width = 1
	public set width(width: number) {
		this._width = width;
		this.updateSize();
	}
	
	private _height = 1;
	public set height(height: number) {
		this._height = height;
		this.updateSize();
	}
	
	private _tileSize = 14;
	public set tileSize(tileSize: number) {
		this._tileSize = tileSize;
		this.updateSize();
	}
	
	private _wallSize = 2;
	public set wallSize(wallSize: number) {
		this._wallSize = wallSize;
		this.updateSize();
	}
	
	private updateSize() {
		this.canvas.width = this._width * this._tileSize + (this._width + 1) * this._wallSize;
		this.canvas.height = this._height * this._tileSize + (this._height + 1) * this._wallSize;
		this.maze = new MultiArray(this._width * 2 - 1, this._height * 2 - 1);
	}
	
	private _colours: (c: T) => string = (c: T) => c ? "#777" : "#FFF";
	public set colours(colours: Map<T, string> | ((c: T) => string)) {
		if (typeof colours !== 'function') {
			const map = colours;
			colours = (c: T) => map.get(c) ?? "#FFF";
		}
		this._colours = colours;
	}
	
	public fill(value: T) {
		this.maze.fill(value);
		this.ctx.fillStyle = this._colours(value);
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
	}
	
	public setRaw(value: T, x: number, y: number) {
		this.maze.set(value, x, y);
		
		this.ctx.fillStyle = this._colours(value);
		
		const wx = x % 2; x = Math.floor(x / 2);
		const wy = y % 2; y = Math.floor(y / 2);
		this.ctx.fillRect(
			(x + wx) * this._tileSize + (x + 1) * this._wallSize,
			(y + wy) * this._tileSize + (y + 1) * this._wallSize,
			wx ? this._wallSize : this._tileSize,
			wy ? this._wallSize : this._tileSize,
		);
	}
	
	public setCell(value: T, x: number, y: number) {
		this.maze.set(value, x*2, y*2);
		
		this.ctx.fillStyle = this._colours(value);
		this.ctx.fillRect(
			x * this._tileSize + (x + 1) * this._wallSize,
			y * this._tileSize + (y + 1) * this._wallSize,
			this._tileSize,
			this._tileSize,
		);
	}
	
	public addWall(value: T, x: number, y: number, [dx, dy]: Direction) {
		this.ctx.fillStyle = this._colours(value);
		
		if (dx != 0) {
			this.maze.set(value, x*2 + dx, y*2 - 1);
			this.maze.set(value, x*2 + dx, y*2);
			this.maze.set(value, x*2 + dx, y*2 + 1);
			
			const wx = x + (dx + 1) / 2;
			this.ctx.fillRect(
				wx * this._tileSize + wx * this._wallSize,
				y * this._tileSize + y * this._wallSize,
				this._wallSize,
				this._tileSize + 2 * this._wallSize,
			);
		} else { // dir[1] != 0
			this.maze.set(value, x*2 - 1, y*2 + dy);
			this.maze.set(value, x*2, y*2 + dy);
			this.maze.set(value, x*2 + 1, y*2 + dy);
			
			const wy = y + (dy + 1) / 2;
			this.ctx.fillRect(
				x * this._tileSize + x * this._wallSize,
				wy * this._tileSize + wy * this._wallSize,
				this._tileSize + 2 * this._wallSize,
				this._wallSize,
			);
		}
	}
	
	public clearWall(value: T, x: number, y: number, [dx, dy]: Direction) {
		this.ctx.fillStyle = this._colours(value);
		
		if (dx != 0) {
			if (dx < 0) x += dx;
			
			this.maze.set(value, x*2, y*2);
			this.maze.set(value, x*2 + 1, y*2);
			this.maze.set(value, x*2 + 2, y*2);
			
			this.ctx.fillRect(
				x * this._tileSize + (x + 1) * this._wallSize,
				y * this._tileSize + (y + 1) * this._wallSize,
				2 * this._tileSize + this._wallSize,
				this._tileSize,
			);
		} else { // dir[1] != 0
			if (dy < 0) y += dy;
			
			this.maze.set(value, x*2, y*2);
			this.maze.set(value, x*2, y*2 + 1);
			this.maze.set(value, x*2, y*2 + 2);
			
			this.ctx.fillRect(
				x * this._tileSize + (x + 1) * this._wallSize,
				y * this._tileSize + (y + 1) * this._wallSize,
				this._tileSize,
				2 * this._tileSize + this._wallSize,
			);
		}
	}
	
	public inMaze(x: number, y: number) {
		return (
			x >= 0 && x < this._width &&
			y >= 0 && y < this._height
		);
	}
	
	public getRaw(x: number, y: number) {
		return this.maze.get(x, y);
	}
	
	public getCell(x: number, y: number) {
		return this.inMaze(x, y) ? this.maze.get(x*2, y*2) : undefined;
	}
	
	public getWall(x: number, y: number, [dx, dy]: Direction) {
		return this.inMaze(x, y) && this.inMaze(x + dx, y + dy) ?
			this.maze.get(x*2 + dx, y*2 + dy) : undefined;
	}
	
	constructor() {
		super();
		
		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d')!;
		
		this.maze = new MultiArray<T, 2>(0, 0);
		
		this.shadow.appendChild(this.canvas);
	}
}

export type MazeConstructor<T = boolean> = CustomElement<MazeElement<T>, MazeProperties<T>>;

export default customElement<MazeElement<unknown>, MazeProperties<unknown>>('x-maze', MazeElement) as <T>(props: MazeProperties<T>) => MazeElement<T>;
