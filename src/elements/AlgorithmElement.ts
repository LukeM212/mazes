import {CustomElement} from '../lib/customElement';
import {SettingValues} from './SettingsElement';
import MazeConstructor, {MazeElement} from './MazeElement';

export type AlgorithmProperties = {settings: SettingValues};

export abstract class AlgorithmElement<T = boolean> extends HTMLElement {
	private readonly shadow: ShadowRoot = this.attachShadow({mode: 'closed'});
	
	protected maze: MazeElement<T>;
	private state: Generator<undefined, void, never>;
	
	private readonly stepButton: HTMLButtonElement;
	private readonly step10Button: HTMLButtonElement;
	private readonly playButton: HTMLButtonElement;
	private readonly finishButton: HTMLButtonElement;
	
	private interval?: number;
	
	public set settings(settings: SettingValues) {
		this.data = settings;
		
		const oldMaze = this.maze;
		this.maze = this.createMaze();
		this.shadow.replaceChild(this.maze, oldMaze);
		
		this.state = this.run();
		this.step();
	}
	
	protected createMaze() {
		return MazeConstructor<T>({width: this.width, height: this.height});
	}
	
	protected data: SettingValues = {};
	
	protected get width() { return this.data.width as number; }
	protected get height() { return this.data.height as number; }
	
	constructor() {
		super();
		
		this.maze = MazeConstructor({width: 1, height: 1});
		this.shadow.appendChild(this.maze);
		
		this.shadow.appendChild(document.createElement('br'));
		
		this.stepButton = document.createElement('button');
		this.stepButton.innerText = "Step";
		this.stepButton.onclick = this.step.bind(this, 1);
		this.shadow.appendChild(this.stepButton);
		
		this.step10Button = document.createElement('button');
		this.step10Button.innerText = "Step 10";
		this.step10Button.onclick = this.step.bind(this, 10);
		this.shadow.appendChild(this.step10Button);
		
		this.playButton = document.createElement('button');
		this.playButton.innerText = "Play";
		this.playButton.onclick = this.play.bind(this);
		this.shadow.appendChild(this.playButton);
		
		this.finishButton = document.createElement('button');
		this.finishButton.innerText = "Finish";
		this.finishButton.onclick = this.step.bind(this, Infinity);
		this.shadow.appendChild(this.finishButton);
		
		this.state = this.run();
	}
	
	private step(n: number = 1) {
		for (let i = 0; i < n; i++) {
			if (this.state.next().done) {
				if (this.interval !== undefined) this.pause();
				this.stepButton.disabled = true;
				this.step10Button.disabled = true;
				this.playButton.disabled = true;
				this.finishButton.disabled = true;
				break;
			}
		}
	}
	
	private play() {
		this.interval = setInterval(this.step.bind(this, 1), 500);
		this.playButton.innerText = "Pause";
		this.playButton.onclick = this.pause.bind(this);
	}
	
	private pause() {
		clearInterval(this.interval);
		this.interval = undefined;
		this.playButton.innerText = "Play";
		this.playButton.onclick = this.play.bind(this);
	}
	
	abstract run(): Generator<undefined, void, undefined>;
}

export type AlgorithmConstructor<T> = CustomElement<AlgorithmElement<T>, AlgorithmProperties>;
