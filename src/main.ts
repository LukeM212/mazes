import SettingsElement, {Settings, Options} from './elements/SettingsElement';
import {AlgorithmConstructor} from './elements/AlgorithmElement';
import LazyObj from './lib/LazyObj';
import importJSON from './lib/importJSON';

type AlgorithmDefinitions = Obj<AlgorithmDefinition>;

type AlgorithmDefinition = {
	name: string,
	settings: Settings,
};

async function topLevel() {
	const definitions: AlgorithmDefinitions = await importJSON('./algorithms.json');
	
	const algorithms = new LazyObj<AlgorithmConstructor<unknown>>(
		async key => (await import(`./algorithms/${key}.js`)).default
	);
	
	const algorithmSettings: Options = {};
	
	for (const key in definitions) {
		const {name, settings} = definitions[key];
		algorithmSettings[key] = {label: name, content: settings};
	}
	
	const settingsElement = SettingsElement({
		settings: {
			width: {
				type: 'number',
				label: "Width: ",
				min: 1,
				value: 10
			},
			height: {
				type: 'number',
				label: "Height: ",
				min: 1,
				value: 10
			},
			algorithm: {
				type: 'options',
				label: "Algorithm: ",
				options: algorithmSettings
			}
		}
	});
	
	document.body.appendChild(settingsElement);
	
	let content: HTMLElement = document.createElement('p');
	content.innerText = "Press [Start] to visualise a maze generation algorithm";
	
	const button = document.createElement('button');
	button.innerText = "Start";
	button.onclick = async () => {
		button.disabled = true;
		
		const settings = settingsElement.values;
		
		const algorithm = (await algorithms.get(settings.algorithm as string))({settings});
		document.body.replaceChild(algorithm, content);
		content = algorithm;
		
		button.disabled = false;
	};
	document.body.appendChild(button);
	
	document.body.appendChild(document.createElement('br'));
	
	document.body.appendChild(content);
}

topLevel();
