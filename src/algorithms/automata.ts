import {customElement} from '../lib/customElement';
import {AlgorithmElement, AlgorithmProperties} from '../elements/AlgorithmElement';
import MultiArray from '../lib/MultiArray';
import MazeConstructor from '../elements/MazeElement';
import {All as Dirs} from '../lib/Direction';

type Cell = 0 | 1 | 2;

const pBranch = 1 / 64;

function update(get: (relX: number, relY: number) => Cell): [Cell, boolean] {
	const cell = get(0, 0);
	
	if (cell === 2) return [2, true];
	
	let surrounding = 0, done = true;
	
	loop: for (const [dx, dy] of Dirs) {
		switch (get(dx, dy)) {
			case 1:
				if (get(2*dx, 2*dy) === 2)
					surrounding += 1;
				break;
			case 2:
				if (get(-dx, -dy) !== 2) {
					if (
						get(-dx - dy, -dy - dx) === 1 ||
						get(-2*dx, -2*dy) === 1 ||
						get(-dx + dy, -dy + dx) === 1
					) return [0, cell === 0];
					if (cell === 1) return [1, true];
					else if (Math.random() < pBranch) return [1, false];
					else done = false;
				}
				break loop;
		}
	}
	
	if (surrounding === 1) return [2, false];
	
	return [cell, done];
}

export class AutomataElement extends AlgorithmElement<Cell> {
	*run() {
		this.maze.fill(0);
		this.maze.setCell(2, Math.floor(this.width / 2), Math.floor(this.height / 2));
		
		const automataWidth = this.width * 2 - 1;
		const automataHeight = this.height * 2 - 1;
		
		let buffer = new MultiArray<Cell, 2>(automataWidth, automataHeight);
		
		let done = false;
		while (!done) {
			yield;
			
			const tmp = this.maze.maze;
			this.maze.maze = buffer;
			buffer = tmp;
			
			done = true;
			
			for (let x = 0; x < automataWidth; x++) {
				for (let y = 0; y < automataHeight; y++) {
					const [newCell, cellDone] = update((relX, relY) => buffer.get(x + relX, y + relY) ?? 0);
					this.maze.setRaw(newCell, x, y);
					if (!cellDone) done = false;
				}
			}
		}
	}
	
	protected createMaze() {
		return MazeConstructor<Cell>({
			width: this.width,
			height: this.height,
			tileSize: 8,
			wallSize: 8,
			colours: new Map([[0, "#777"], [1, "#FFF"], [2, "#FFF"]]),
		});
	}
}

export default customElement<AutomataElement, AlgorithmProperties>('x-automata', AutomataElement);
