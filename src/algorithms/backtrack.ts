import {customElement} from '../lib/customElement';
import {AlgorithmElement, AlgorithmProperties} from '../elements/AlgorithmElement';
import Direction, {All as Dirs} from '../lib/Direction';

export class BacktrackElement extends AlgorithmElement {
	*run() {
		let x = 0, y = 0;
		const stack: Direction[] = [];
		let done: boolean = false;
		
		this.maze.fill(true);
		this.maze.setCell(false, x, y);
		
		while (!done) {
			const dirs = Dirs.filter(([dx, dy]) => this.maze.getCell(x + dx, y + dy) === true);
			
			if (dirs.length > 0) {
				yield;
				const dir = dirs[Math.floor(Math.random() * dirs.length)];
				this.maze.clearWall(false, x, y, dir);
				const [dx, dy] = dir;
				x += dx; y += dy;
				stack.push(dir);
			} else {
				const dir = stack.pop();
				
				if (dir === undefined) done = true;
				else {
					const [dx, dy] = dir;
					x -= dx; y -= dy;
				}
			}
		}
	}
}

export default customElement<BacktrackElement, AlgorithmProperties>('x-backtrack', BacktrackElement);
